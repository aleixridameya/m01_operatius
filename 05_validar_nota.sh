#! /bin/bash
# @aleixridameya ASIX-M01
# Febrer 2023
#
# Dir si la nota és un susper o un aprovat
# ----------------------------------------------------------------
# 1) validar que existeix un argument
if [ $# -ne 1 ] 
then
  echo "ERROR: numero d'arguments incorrecte"
  echo "USAGE: $0 nota"
  exit 1
fi

# 2) validar numero sigui correcte
nota=$1

if [ $nota -gt 10 ]
then
  echo "ERROR: numero de la nota $nota incorrecte"
  echo "numero de la nota ha de ser entre 0-10"
  echo "USAGE: $0 nota"
  exit 2
fi

# 3) Xixa

if [ $nota -lt 5 ] 
then
  echo "Suspes"
else 
  echo "aprovat"
fi

