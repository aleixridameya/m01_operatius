#! /bin/bash
# @aleixridameya ASIX-M01
# Març 2023
# 01_exercici.sh
# Processar arg, mostra amb mes de 4 char.
# -----------------------------------------------------

# xixa
arg_list=$*

for arg in $arg_list
do
  echo "$arg" | grep -E "^.{4,}" 
done
exit 0

