#! /bin/bash
# @aleixridameya ASIX-M01
# Març 2023
# 03_exercici.sh
# Processar arg que son matricula "9999-AAA" correcte stdout, incorrecte stderr
# ----------------------------------------------------

#Xixa
matricula_list=$*
error=0
for matricula in $matricula_list
do
  echo "$matricula" | grep -E -q "^[0-9]{4}-[A-Z]{3}$"
  if [ $? -eq 0 ] ; then
    echo "$matricula"
  else
    echo "$matricula" >> /dev/stderr
    ((error++))
  fi
done
exit $error

