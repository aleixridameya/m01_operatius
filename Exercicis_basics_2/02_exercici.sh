#! /bin/bash
# @aleixridameya ASIX-M01
# Març 2023
# 01_exercici.sh
# Processar arg, i compta quants hi de 3 o mes
# ----------------------------------------------------


# Xixa 

list_arg=$*
cont=0

for arg in $list_arg
do
  echo "$arg" | grep -E -q "^.{3,}"
  if [ $? -eq 0 ] ; then
    ((cont++))
  fi
done
echo "Hi ha $cont args de 3 o mes caràcters"
