#! /bin/bash
# @aleixridameya ASIX-M01
# Febrer 2023
#
# Exemple de if: inidicar si és major d'edat
# $ prog edat
# ----------------------------------------------------------------
# 1) validar que existeix un argument
if [ $# -ne 1 ]
then
  echo "Error: numero args incorrecte"
  echo "Usage: $0 edat"
  exit 1
fi

# 2) xixa
edat=$1

if [ $edat -lt 18 ]
then
  echo "edat $edat és manor d'edat"
elif [ $edat -ge 65 ]
then
  echo "edat $edat és un jubilat"
else
  echo "edat $edat es major d'edat"

fi

exit 0

