#! /bin/bash
# @aleixridameya ASIX-M01
# Març 2023
# Sinopsis: prog dir
# Validar que es rep un argment i que és un directori i llistar-ne el contingut- per llistar el continguy amb un simple ls ja n'hi ha prou
# -----------------------------------------------------

# validar arg
ERR_ARG=1
ERR_DIR=2
if [ $# -lt 1 ] ; then
 echo "ERROR: numero d'arguments erroni"
 echo "Usage: $0 dir..."
 exit $ERR_ARG
fi

#Xixa
llista_dir=$*

for dir in $llista_dir
do
  if ! [ -d $dir ] ; then
     echo "Error: el $dir no es un directori" >> /dev/stderr
  else 
    echo "El dir es $dir"
    llista=$(ls $dir)    
    for elem in $llista
    do
      if [ -h "$dir/$elem" ] ; then
        echo -e "\t El element: $elem és un link"
      elif [ -f "$dir/$elem" ] ; then
        echo -e "\t El element: $elem és un regular file"
      elif [ -d "$dir/$elem" ] ; then 
        echo -e "\t El element: $elem és un directori"
      else 
        echo "El elemt: $elem és una altre cosa"
      fi
    done
fi
done
exit 0
