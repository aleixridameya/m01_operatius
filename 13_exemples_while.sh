#! /bin/bash
# @aleixridameya ASIX-M01
# Març 2023
#
# Exemples while
# -----------------------------------------------------

# 7) numera i mostra en majuscules l'entrada estàndard
num=1
while read -r line
do  
  mayus=$(echo "$line" | tr 'a-z' 'A-Z')	
  echo "$((num++)): $mayus"
done
exit 0


# 6) Postrar stfin linia a linia fins token FI
read -r line
while  [ $line != 'FI' ] 
do
  echo "$line"
  read -r line
done
exit 0

# 5) Numerar stdin linia a linia
num=1
while read -r line
do
 echo "$((num++)): $line"
done
exit 0

# 4) Processar entrada estandard linia a linia

while read -r line
do 
  echo "$line"
done
exit 0

# 3) Iterar per la llista d'arguments

while [ -n "$1"  ]
do
  echo "$1 $# $*"
  shift
done
exit 0

# 2) Comptador decreixent n(arg)...0

num=$1
MIN=0
while [ $num -ge $MIN ]
do
  echo "$((num--))"
done
exit 0

# 1) Mostra numero del 1 al 10
num=1
MAX=10
while [ $num -le $MAX ]
do
  echo "$((num++))"
done
exit 0
