#! /bin/bash
# @aleixridameya ASIX-M01
# Març 2023
# Sinopsis: prog arg... 
# separa opcions i arguments
# -----------------------------------------------------




# for

for arg in $*
do
  case $arg in
	  "-a"|"-b"|"-c"|"-d")
	opcions="$opcions $arg"
	;;
 	*)
	arguments="$arguments $arg"
	;;
 esac
done
echo -e "opcions: $opcions \narguments: $arguments"
exit 0
