#! /bin/bash
# @aleixridameya ASIX-M01
# Març 2023
# 02_exercici.sh
# Mostra els arguments rabuts linia a linia numerats
# -----------------------------------------------------

num=1
list_arg=$*
for arg in $list_arg
do
  echo "$num: $arg"
  ((num++))
done
exit 0
