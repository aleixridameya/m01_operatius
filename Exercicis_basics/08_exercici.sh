#! /bin/bash
# @aleixridameya ASIX-M01
# Març 2023
# 08_exercici.sh
# Mostra els arguments rabuts linia a linia numerats
# -----------------------------------------------------

#xixa
list_user=$*

for user in $list_user
do 
  grep -q "^$user:" /etc/passwd
  if [ $? -eq 0 ] ; then
     echo "$user" 
  else
    echo "$user" >> /dev/stderr

  fi
done
exit 0
