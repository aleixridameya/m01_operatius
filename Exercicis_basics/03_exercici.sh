#! /bin/bash
# @aleixridameya ASIX-M01
# Març 2023
# 03_exercici.sh
# De 0 fins numero indicat en el argument
# -----------------------------------------------------

#xixa
MAX=$1
num=0
while [ $num -le $MAX ] 
do
  echo "$num"
  ((num++))
done
exit 0

