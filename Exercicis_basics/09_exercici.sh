#! /bin/bash
# @aleixridameya ASIX-M01
# Març 2023
# 09_exercici.sh
# Mostra els usuaris per stdin 
# -----------------------------------------------------

#xixa
while read -r line
do 
  grep -q "^$line:" /etc/passwd
  if [ $? -eq 0 ] ; then
     echo "$line" 
  else
    echo "$line" >> /dev/stderr

  fi
done
exit 0

