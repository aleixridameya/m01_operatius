#! /bin/bash
# @aleixridameya ASIX-M01
# Març 2023
# 02_exercici.sh
# mostra dies laborals, festius, resta de coses error ( 1 o mes args)
# -----------------------------------------------------

# xixa
dia_semana_list=$*
festiu=0
laboral=0

for dia_semana in $dia_semana_list
do
case $dia_semana in
	"dissabte"|"diumenge")
	((festiu++))
	;;
       "dilluns"|"dimarts"|"dimecres"|"dijous"|"divendres")
	((laboral++))
	;;
	*)
	echo "Error: $dia_semana no es un dia de la setmana" >> /dev/stderr
	;;
esac
done
echo "Dies festius: $festiu"
echo "Dies laborables: $laboral"
exit 0

