#! /bin/bash
# @aleixridameya ASIX-M01
# Març 2023
# 09_exercici.sh
# Mostrar que rep com argument maxim de linies a mostrar i han d'estar numerades
# -----------------------------------------------------


# xixa
MAX=$1
num=0

while read -r line
do
  if [ $MAX -gt $num ] ; then
    ((num++))
    echo "$num: $line" 
  fi
done
exit 0
