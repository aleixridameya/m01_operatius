#! /bin/bash
# @aleixridameya ASIX-M01
# Març 2023
# 07_exercici.sh
# Mostra linies mac de 60 caracters
# -----------------------------------------------------

# versio grep
while read -r line
do
  echo "$line" | grep -E "^.{60,}"
done
exit 0


# xixa
while read -r line
do
  caracters=$(echo "$line" | wc -c)
  if [ $caracters -gt 60 ] ; then
     echo "$line"
  fi
done
exit 0


