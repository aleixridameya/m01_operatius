#! /bin/bash
# @aleixridameya ASIX-M01
# Març 2023
# 05_exercici.sh
# Mostra els arguments de la entrada estandard coma maxim 50 caracters 
# -----------------------------------------------------

while read -r line
do  
  echo "$line" | cut -c1-50
done
exit 0
