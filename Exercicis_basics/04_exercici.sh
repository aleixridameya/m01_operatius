#! /bin/bash
# @aleixridameya ASIX-M01
# Març 2023
# 02_exercici.sh
# mostra quants dies te mes (1 o mes arguments)
# -----------------------------------------------------


# xixa 
llista_mesos=$*

for mes in $llista_mesos
do	
case $mes in 
	"2")
	dies=28	
	;;
       "4"|"6"|"9"|"11")
	dies=30
	;;
	*)
	dies=31
	;;
esac
echo "el mes $mes te $dies dies"
done
exit 0
