#! /bin/bash
# @aleixridameya ASIX-M01
# Febrer 2023
#
# Exemple de processar arguments
# ----------------------------------------------------------------
echo '$*: ' $* # Llista d'arguments
echo '$@: ' $@ # Llista d'arguments
echo '$#: ' $# # Num d'arguments
echo '$0: ' $0 #nom del programa
echo '$1: ' $1 #primer argument
echo '$2: ' $2 #segon argument
echo '$9: ' $9
echo '$10: ' ${10}
echo '$11: ' ${11}
echo '$$: ' $$ #pid del programa
nom="puig"
echo "${nom}deworld"


