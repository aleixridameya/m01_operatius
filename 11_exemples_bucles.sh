#! /bin/bash
# @aleixridameya ASIX-M01
# Març 2023
#
# Exemples bucle
# -----------------------------------------------------
	

# 8) LListar tots els logins
list_login=$(cut -d':' -f1 /etc/passwd | sort)

for log in $list_login
do
  echo "$log"
done
exit 0

# 7) llistar numerats els noms dels files del directoria atiu
llistat=$(ls)
num=1
for nom in $llistat
do 
  echo "$num: $nom"
  ((num++))
done
exit 0



# 5) numerar arguments
args=$@
contador=1
for arg in $args
do
  echo "$contador: $arg"
  ((contador++))
done
exit 0


# 4) Iterar i mostrar la llista d'arguments
args=$@
for arg in $args
do
  echo "$arg"
done
exit 0

# 3) Iterar pel valor d'una variable
llistat=$(ls)

for nom in $llistat
do 
  echo "$nom"
done
exit 0

# 2) Iterar per un conjunt d'elements
for nom in "pere mart pau anna"
do 
  echo "$nom"
done
exit 0

# 1) Iterar per un conjunt d'elements
for nom in "pere" "marta" "pau" "anna"
do 
  echo "$nom"
done
exit 0


