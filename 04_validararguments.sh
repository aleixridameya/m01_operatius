#! /bin/bash
# @aleixridameya ASIX-M01
# Febrer 2023
#
# Validar arguments: mostrar el nom i l'edat
# ----------------------------------------------------------------
# 1) validar que existeix un argument
if [ $# -ne 2 ] 
then
  echo "Error: numero d'arguments incorrecte"
  echo "Usage: $0 nom edat"
  exit 1
fi
#) 2 xixa
echo "nom: $1"
echo "edat: $2"


