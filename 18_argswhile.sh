#! /bin/bash
# @aleixridameya ASIX-M01
# Març 2023
# Sinopsis: prog arg... 
# separa opcions i arguments
# -----------------------------------------------------


opcions=""
arguments=""

while [ -n "$1" ]
do
  case $1 in
	  -[abcd])
	opcions="$opcions $1"
	;;
 	  "-f")
	file=$2
	shift
	;;
	 "-n")
	num=$2
	shift
	;;
	*)
	arguments="$arguments $1"
	;;
  esac
shift
done
echo -e "opcions: $opcions \narguments: $arguments \nfile: $file \nnum: $num"
exit 0
