#! /bin/bash
# @aleixridameya ASIX-M01
# Març 2023
#
# Suma de numeros
# -----------------------------------------------------
ERR_ARGS=1

# validació d'arguments
if [ $# -eq 0 ] ; then
  echo "ERROR: numero d'argumnts incorrecte"
  echo "USAGE: $0 numeo,..."
  exit $ERR_ARGS
fi

# xixa
llista_numero=$*
suma=0
for num in $llista_numero
do 
  suma=$((suma+num))
done
echo "Resultat suma: $suma"
exit 0
