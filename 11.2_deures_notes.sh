#! /bin/bash
# @aleixridameya ASIX-M01
# Març 2023
#
# Dir si ha tret suspes,aprovat, notable, excelent
# -----------------------------------------------------
ERR_ARGS=1

# validació d'arguments
if [ $# -eq 0 ] ; then
  echo "ERROR: numero d'argumnts incorrecte"
  echo "USAGE: $0 nota,..."
  exit $ERR_ARGS
fi

# Xixa
notes_list=$*

for nota in $notes_list
do
  if [ $nota -lt 0 -o $nota -gt 10 ] ; then
     echo "ERROR: el numero de la nota $nota no és vàlida. Numero valid 0-10" >> /dev/stderr
  elif [ $nota -lt 5 ] ; then
     echo "La teva nota $nota és un suspés"
  elif [ $nota -lt 7 ] ; then
     echo "La teva nota $nota és un aprovat"
  elif [ $nota -lt 9 ] ; then 
     echo "La teva nota $nota és un notable"
   else
     echo "La teva nota $nota és un excel·lent"
fi
done
exit 0
