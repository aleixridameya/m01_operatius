#! /bin/bash
# @aleixridameya ASIX-M01
# Febrer 2023
#
# Dir si l'argument és un regular file, dir, link 
# ----------------------------------------------------------------
# 1) validar que existeix un argument
ERR_ARGS=1
ERR_NOEXIST=2
if [ $# -ne 1 ] 
then
  echo "ERROR: numero d'arguments incorrecte"
  echo "USAGE: $0 directori"
  exit $ERR_ARGS
fi

# xixa
fitxer=$1
if [ ! -e $fitxer ]
then
  echo "$fitxer no existeix"
  exit $ERR_NOEXIST
elif [ -d $fitxer ]
then
 echo "el fitxer $fitxer és un directori"
elif [ -h $fitxer ] 
then
  echo "el fitxer $fitxer és un link"
elif [ -f $fitxer ] 
then 
  echo "el fitxer $fitxer és un regular file"
else
  echo "el fitxer $fitxer és una altre cosa"
fi

exit 0

