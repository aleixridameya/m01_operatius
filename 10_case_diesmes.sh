#! /bin/bash
# @aleixridameya ASIX-M01
# Febrer 2023
#
# Programa que posem un mes i ens ha de dir quants dies te
# --------------------------------------------------------------
# validar numero d'aeguments
ERR_ARGS=1
ERR_NUM=2

if [ $# -ne 1 ] ; then
  echo "ERROR: numero d'argumnts incorrecte"
  echo "USAGE: $0 numero_mes"
  exit $ERR_ARGS
fi
# validar que es un numero entre 1-12
if [ $1 -gt 12 -o $1 -lt 1 ] ; then
  echo "ERROR: el numero $1 ha de ser entre 1-12"
  exit $ERR_NUM
fi
# xixa
num_mes=$1

case $num_mes in
	"2")
	echo "Aquest mes $num_mes te 28 dies"
	;;
	"4"|"6"|"9"|"11")
	echo "Aquest mes $num_mes te 30 dies"
	;;
	*)
	echo "Aquest mes $num_mes te 31 dies"
	;;
esac
exit 0
