#! /bin/bash
# @aleixridameya ASIX-M01
# Març 2023
# Sinopsis: prog dir
# Validar que es rep un argment i que és un directori i llistar-ne el contingut- per llistar el continguy amb un simple ls ja n'hi ha prou
# -----------------------------------------------------

# validar arg
ERR_ARG=1
ERR_DIR=2
if [ $# -ne 1 ] ; then
 echo "ERROR: numero d'arguments erroni"
 echo "Usage: $0 dir"
 exit $ERR_ARG
fi

#Validar si es dir
dir=$1
if ! [ -d $dir ] ; then
  echo "Error: el $dir no es un directori" 
  exit $ERR_DIR  
fi

#Xixa

llista=$(ls $dir)
num=1
for elem in $llista
do
  echo "$num: $elem"
  ((num++))
done
exit 0
