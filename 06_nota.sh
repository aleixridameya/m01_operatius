#! /bin/bash
# @aleixridameya ASIX-M01
# Febrer 2023
#
# Dir si la nota és un susper, aprovat, notable, excel·lent
# ----------------------------------------------------------------
# 1) validar que existeix un argument
if [ $# -ne 1 ] 
then
  echo "ERROR: numero d'arguments incorrecte"
  echo "USAGE: $0 nota"
  exit 1
fi

# 2) validar numero sigui correcte
nota=$1

if [ $nota -gt 10 ]
then
  echo "ERROR: numero de la nota $nota incorrecte"
  echo "numero de la nota ha de ser entre 0-10"
  echo "USAGE: $0 nota"
  exit 2
fi

# 3) Xixa

if [ $nota -lt 5 ] 
then
  echo "nota $nota: Suspes"
elif [ $nota -lt 7 ]  
then
  echo "nota $nota: aprovat"
elif [ $nota -lt 9 ] 
then
  echo "nota $nota: notable"
else
  echo "nota $nota: excel·lent"
fi

exit 0
