#! /bin/bash
# @aleixridameya ASIX-M01
# Febrer 2023
#
# Fer un ls d'un directori 
# ----------------------------------------------------------------
# 1) validar que existeix un argument
ERR_ARGS=1
ERR_DIR=2

if [ $# -ne 1 ] 
then
  echo "ERROR: numero d'arguments incorrecte"
  echo "USAGE: $0 directori"
  exit $ERR_ARGS
fi

# Validar si es dr
if ! [ -d $1 ]
then
  echo "ERROR: argument incorrecte, ha de ser un directori."
  echo "USAGE: $0 directori"
  exit $ERR_DIR
fi

#xixa
directori=$1

ls $directori

exit 0
